#!/usr/bin/bash

# usage
#  $ sh buildAll.sh production

BASE_DIR=$(cd $(dirname $0); pwd)
ENV=$1 # staging or production

echo "scop-home-server-apiの全ビルドを開始します"
date

# scop-now-server-api build
BACK_END_MODULE=scop-home-server-api-0.0.1-SNAPSHOT.war
sh $BASE_DIR/scop-home-server-api/build.sh $BASE_DIR/scop-home-server-api/ $ENV $BACK_END_MODULE

echo "scop-home-server-apiの全ビルドを終了します"
date
