SET CHARSET utf8;
CREATE TABLE IF NOT EXISTS company(
	companyId int primary key AUTO_INCREMENT, 
	companyName varchar(30),
	jobCategoryLevel1 varchar(2),
	jobCategoryLevel2 varchar(2),
	foundingDate varchar(10),
	employeeNumber int(2),
	registDate datetime,
	registUser varchar(10),
	updateDate datetime,
	updateUser varchar(10)
) DEFAULT CHARSET=utf8;
