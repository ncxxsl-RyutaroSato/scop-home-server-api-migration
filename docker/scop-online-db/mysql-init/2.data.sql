SET CHARACTER_SET_CLIENT = utf8;
SET CHARACTER_SET_CONNECTION = utf8;

INSERT INTO company(companyId,companyName,jobCategoryLevel1, jobCategoryLevel2, foundingDate, employeeNumber, registDate, registUser, updateDate, updateUser) values (1, 'A社', '01', '01', '2018/01/01', 10, '2018-01-01 00:00:00', 'U001', '2018-01-01 00:00:00', 'U001');
INSERT INTO company(companyId,companyName,jobCategoryLevel1, jobCategoryLevel2, foundingDate, employeeNumber, registDate, registUser, updateDate, updateUser) values (2, 'B社', '01', '02', '1970/01/01', 1000, '2018-01-01 00:00:00', 'U001', '2018-01-01 00:00:00', 'U001');
INSERT INTO company(companyId,companyName,jobCategoryLevel1, jobCategoryLevel2, foundingDate, employeeNumber, registDate, registUser, updateDate, updateUser) values (3, 'C社', '01', '03', '1995/01/01', 300, '2018-01-01 00:00:00', 'U001', '2018-01-01 00:00:00', 'U001');
INSERT INTO company(companyId,companyName,jobCategoryLevel1, jobCategoryLevel2, foundingDate, employeeNumber, registDate, registUser, updateDate, updateUser) values (4, 'D社', '02', '01', '2000/01/01', 5, '2018-01-01 00:00:00', 'U001', '2018-01-01 00:00:00', 'U001');
INSERT INTO company(companyId,companyName,jobCategoryLevel1, jobCategoryLevel2, foundingDate, employeeNumber, registDate, registUser, updateDate, updateUser) values (5, 'D社', '02', '02', '2015/01/01', 100, '2018-01-01 00:00:00', 'U001', '2018-01-01 00:00:00', 'U001');
INSERT INTO company(companyId,companyName,jobCategoryLevel1, jobCategoryLevel2, foundingDate, employeeNumber, registDate, registUser, updateDate, updateUser) values (6, 'D社', '02', '03', '1980/01/01', 3, '2018-01-01 00:00:00', 'U001', '2018-01-01 00:00:00', 'U001');
