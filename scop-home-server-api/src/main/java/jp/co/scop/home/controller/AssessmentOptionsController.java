package jp.co.scop.home.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.scop.home.service.AssessmentOptionsService;

@RestController
@RequestMapping("${system.url}/assessmentOptions")
public class AssessmentOptionsController {

	/** アセスメント項目を取得するためのサービス */
	@Autowired
	AssessmentOptionsService assessmentOptionsService;

	@GetMapping("index")
	public String index() {

		assessmentOptionsService.hoge();
		return "assessmentOptions/index";
	}
	
}
