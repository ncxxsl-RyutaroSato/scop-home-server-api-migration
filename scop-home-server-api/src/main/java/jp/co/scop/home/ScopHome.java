package jp.co.scop.home;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("jp.co.scop.home")
@ComponentScan("jp.co.scop.common")
public class ScopHome extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ScopHome.class, args);
	} 
	
	 @Override
	 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	 	return application.sources(ScopHome.class);
	 }

}
